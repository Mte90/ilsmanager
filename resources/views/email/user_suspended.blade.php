<p>
    Siamo spiacenti di informarti che la tua iscrizione a {{ App\Config::getConfig('association_name') }} è stata sospesa.
</p>
<p>
    Per eventuali domande o dubbi puoi scrivere all'indirizzo email {{ App\Config::getConfig('association_email') }}
</p>
