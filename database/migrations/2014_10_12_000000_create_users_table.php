<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->rememberToken();
            $table->timestamps();

            $table->string('username')->unique();
            $table->string('password');
            $table->enum('status', ['pending', 'active', 'suspended', 'expelled', 'dropped']);
            $table->enum('type', ['regular', 'association']);

            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('website')->nullable();
            $table->string('taxcode')->nullable();
            $table->text('notes');

            $table->string('birth_place')->nullable();
            $table->string('birth_prov')->nullable();
            $table->date('birth_date')->nullable();

            $table->string('address_street')->nullable();
            $table->string('address_place')->nullable();
            $table->string('address_prov')->nullable();

            $table->date('request_at')->nullable();
            $table->date('approved_at')->nullable();
            $table->date('expelled_at')->nullable();

            $table->integer('section_id')->unsigned()->default(0);
            $table->boolean('volunteer')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
