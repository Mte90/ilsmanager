<p>
    {{ nl2br($votation->question) }}
</p>

<?php $contents = $votation->formatResults() ?>

<ul>
    @foreach($contents as $content)
        <li>
            {{ $content->option }} - {{ $content->count }}
        </li>
    @endforeach
</ul>
