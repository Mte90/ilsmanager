<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountRule extends Model
{
    public function getFixedRuleAttribute()
    {
        if (substr($this->rule, 0, 1) != substr($this->rule, -1, 1)) {
            return sprintf('/%s/', $this->rule);
        }
        else {
            return $this->rule;
        }
    }
}
