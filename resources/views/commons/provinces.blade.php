<select name="{{ $name }}" class="form-control">
    @foreach(allProvinces() as $prov)
        <option value="{{ $prov[0] }}" {{ $value == $prov[0] ? 'selected' : '' }}>{{ $prov[1] }}</option>
    @endforeach
</select>
