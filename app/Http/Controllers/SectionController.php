<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Section;
use App\User;
use App\Role;
use App\Volunteer;

class SectionController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Section',
            'view_folder' => 'section'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'city' => 'required|max:255',
            'prov' => 'required|max:2',
        ];
    }

    protected function requestToObject($request, $object)
    {
        $object->city = $request->input('city');
        $object->prov = $request->input('prov');
        return $object;
    }

    protected function defaultSortingColumn()
    {
        return 'city';
    }

    public function update(Request $request, $id)
    {
        $currentuser = $request->user();
        $section = Section::find($id);
        $this->authorize('update', $section);

        if ($request->has('user')) {
            $users = $request->input('user');
            $roles = $request->input('role');
            $referent_role = Role::where('name', 'referent')->first();

            for ($i = 0; $i < count($users); $i++) {
                $u = $users[$i];
                $r = $roles[$i];

                $user = User::find($u);

                if ($currentuser->hasRole('admin') || $user->section_id == $currentuser->section_id) {
                    if ($r == 'referent') {
                        $user->roles()->attach($referent_role->id);
                    }
                    else {
                        $user->roles()->detach($referent_role->id);
                    }
                }
            }

            return redirect()->route($this->view_folder . '.edit', $id);
        }
        else if ($request->has('volunteer')) {
            $volunteers = $request->input('volunteer');
            $names = $request->input('name');
            $surnames = $request->input('surname');
            $emails = $request->input('email');
            $to_remove = $request->input('remove', []);

            for ($i = 0; $i < count($volunteers); $i++) {
                $v = $volunteers[$i];
                $n = $names[$i];
                $s = $surnames[$i];
                $e = $emails[$i];

                $volunteer = Volunteer::find($v);

                if (in_array($v, $to_remove)) {
                    if ($currentuser->can('delete', $volunteer))
                        $volunteer->delete();
                }
                else {
                    if ($currentuser->can('update', $volunteer)) {
                        $volunteer->name = $n;
                        $volunteer->surname = $s;
                        $volunteer->email = $e;
                        $volunteer->save();
                    }
                }
            }

            return redirect()->route($this->view_folder . '.edit', $id);
        }
        else {
            return parent::update($request, $id);
        }
    }

    public function balance(Request $request, $id)
    {
        $section = Section::find($id);
        $this->authorize('view', $section);

        $year = $request->input('year', date('Y'));
        $rows = $section->economic_logs()->where(DB::raw('YEAR(created_at)'), $year)->orderBy('created_at', 'desc')->get();

        return view('section.balance', compact('section', 'year', 'rows'));
    }
}
