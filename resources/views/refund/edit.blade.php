@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('refund.update', $object->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf

                @include('refund.form', ['object' => $object])

                @if($currentuser->hasRole('admin'))
                    <div class="form-group row">
                        <label for="section_id" class="col-sm-4 col-form-label">Rimborsato</label>
                        <div class="col-sm-8">
                            <div class="btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-success {{ $object->refunded ? 'active' : '' }}">
                                    <input type="checkbox" name="refunded" {{ $object->refunded ? 'checked' : '' }} autocomplete="off"> Rimborsato
                                </label>
                            </div>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            <br>

            @can('delete', $object)
                <form method="POST" action="{{ route('refund.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger">Elimina Richiesta di Rimborso Spese</button>
                        </div>
                    </div>
                </form>
            @endcan
        </div>
    </div>
</div>
@endsection
