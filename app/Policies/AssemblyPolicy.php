<?php

namespace App\Policies;

use App\User;
use App\Assembly;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssemblyPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Assembly $model)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Assembly $model)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Assembly $model)
    {
        return $user->hasRole('admin');
    }
}
