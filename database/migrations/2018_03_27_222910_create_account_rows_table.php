<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('movement_id')->unsigned()->nullable();
            $table->integer('account_id')->unsigned();
            $table->integer('user_id')->unsigned()->default(0);
            $table->integer('section_id')->unsigned()->default(0);
            $table->decimal('amount_in', 10, 2)->default(0);
            $table->decimal('amount_out', 10, 2)->default(0);
            $table->text('notes');

            $table->foreign('movement_id')->references('id')->on('movements')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_rows');
    }
}
