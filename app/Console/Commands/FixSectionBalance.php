<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Section;

class FixSectionBalance extends Command
{
    protected $signature = 'section:balance';
    protected $description = 'Aggiorna il bilancio delle sezioni locali';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $sections = Section::all();
        $threeshold = date('Y-m-d', strtotime('-4 months'));

        foreach($sections as $section) {
            if ($section->created_at > $threeshold) {
                continue;
            }

            if ($section->balance < 0) {
                $section->alterBalance($section->balance * -1, null, 'Aggiornamento di fine anno');
            }
            else {
                $section->alterBalance(round($section->balance / 2, 2) * -1, null, 'Aggiornamento di fine anno');
            }
        }
    }
}
