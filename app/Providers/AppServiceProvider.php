<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\View;

use Auth;
use Theme;

use App\User;
use App\Movement;
use App\AccountRow;
use App\Receipt;
use App\Refund;
use App\Observers\UserObserver;
use App\Observers\MovementObserver;
use App\Observers\AccountRowObserver;
use App\Observers\ReceiptObserver;
use App\Observers\RefundObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*', function($view){
            $view->with('currentuser', Auth::user());
        });

        $theme = config('themes.active');
        if ($theme != 'default') {
            Theme::set($theme);
        }

        Movement::observe(MovementObserver::class);
        AccountRow::observe(AccountRowObserver::class);
        User::observe(UserObserver::class);
        Receipt::observe(ReceiptObserver::class);
        Refund::observe(RefundObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
