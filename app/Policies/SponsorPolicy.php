<?php

namespace App\Policies;

use App\User;
use App\Sponsor;
use Illuminate\Auth\Access\HandlesAuthorization;

class SponsorPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Sponsor $sponsor)
    {
        return $user->hasRole('admin');
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Sponsor $sponsor)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Sponsor $sponsor)
    {
        return $user->hasRole('admin');
    }
}
